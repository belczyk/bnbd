**bnbd** is an alternative NBD server implementation.

Below are some of its key features:

* multi-threaded design
* direct asynchronous I/O over sparse files to access virtual volumes data
* NBD requests and replies pipelining
* AIO requests batching
* concurrent access to virtual volumes from multiple clients
* simple network accessible management CLI
* simple key-value configuration file
* 2-clause BSD license
* clean code base (YMMV)

[![Coverity Scan Build Status][2]][1]

  [1]: https://scan.coverity.com/projects/562
  [2]: https://scan.coverity.com/projects/562/badge.svg
