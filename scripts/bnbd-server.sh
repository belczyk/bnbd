#!/bin/sh

ulimit -c unlimited
ulimit -n 32768
ulimit -s 256

exec ./bnbd-server -C `pwd`/bnbd-server.conf -d $@
